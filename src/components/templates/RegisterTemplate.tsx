import { zodResolver } from '@hookform/resolvers/zod'
import { PATH } from 'constant'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { RegisterSchema, RegisterSchemaType } from 'schema'
import { quanLyNguoiDungServices } from 'services'

const RegisterTemplate = () => {
    const {
        handleSubmit,
        register,
        formState: { errors },
    } = useForm<RegisterSchemaType>({
        mode: 'onChange',
        resolver: zodResolver(RegisterSchema),
    })

    const navigate = useNavigate()

    const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
        try {
            console.log({ value })
            await quanLyNguoiDungServices.register(value)
            toast.success('Đăng ký thành công!')
            navigate(PATH.login)
        } catch (err) {
            toast.error(err?.response?.data?.content)
        }
    }

    return (
        <form className="px-[30px]" onSubmit={handleSubmit(onSubmit)}>
            <h2 className="text-white text-40 font-600">Register</h2>
            <div className="mt-40">
                <input
                    type="text"
                    placeholder="Tài khoản"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    // {...register('taiKhoan', {
                    //     required: 'Vui lòng nhập tài khoản',
                    //     minLength: {
                    //         value: 6,
                    //         message: 'Nhập từ 6 ký tự',
                    //     },
                    //     maxLength: {
                    //         value: 20,
                    //         message: 'Nhập tối đa 20 ký tự',
                    //     },
                    //     // pattern: {
                    //     //     value: ,
                    //     //     message: 'abc'
                    //     // }
                    // })}
                    {...register('taiKhoan')}
                />
                <p className="text-red-500">{errors?.taiKhoan?.message as string}</p>
            </div>
            <div className="mt-40">
                <input
                    type="password"
                    placeholder="Mật khẩu"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    {...register('matKhau')}
                />
                <p className="text-red-500">{errors?.matKhau?.message}</p>
            </div>
            <div className="mt-40">
                <input
                    type="text"
                    placeholder="Email"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    {...register('email')}
                />
                <p className="text-red-500">{errors?.email?.message}</p>
            </div>
            <div className="mt-40">
                <input
                    type="text"
                    placeholder="Số điện thoại"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    {...register('soDt')}
                />
                <p className="text-red-500">{errors?.soDt?.message}</p>
            </div>
            <div className="mt-40">
                <input
                    type="text"
                    placeholder="Mã nhóm"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    {...register('maNhom')}
                />
                <p className="text-red-500">{errors?.maNhom?.message}</p>
            </div>
            <div className="mt-40">
                <input
                    type="text"
                    placeholder="Họ và tên"
                    className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
                    {...register('hoTen')}
                />
                <p className="text-red-500">{errors?.hoTen?.message}</p>
            </div>
            <div className="mt-40">
                <button className="text-white w-full bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">
                    Register
                </button>
            </div>
        </form>
    )
}

export default RegisterTemplate
