import { Menu as MenuA, MenuProps, SubMenuProps } from 'antd'
import MenuItem from 'antd/es/menu/MenuItem'

type MenuObject = {
    (props: MenuProps): JSX.Element
    Item: typeof MenuItem
    SubMenu: React.FC<SubMenuProps>
}

export const Menu: MenuObject = (props) => {
    return <MenuA {...props} />
}

Menu.Item = MenuA.Item
Menu.SubMenu = MenuA.SubMenu
